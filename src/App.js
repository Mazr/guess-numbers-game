import React from 'react';
import GuessForm from "./GuessForm";

class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.refresh = this.refresh.bind(this);
    this.getGuessForm = this.getGuessForm.bind(this);
    this.state = {
      location: "",
      showForm: false
    }
  }

  refresh() {
    location.reload();
    this.setState({showForm:true})
    // this.getGuessForm();
  }

  getGuessForm() {
    if(!this.state.showForm)
    return (<GuessForm location={this.state.location}/>)
  }

  render() {
    return (
      <div>
        <button onClick={this.refresh}>New Game</button>
        {this.getGuessForm()}

      </div>
    )
  }

  componentDidMount() {
    const URL = 'http://localhost:8080/api/games';
    let myHeaders = new Headers();
    let request = new Request(URL, {
      method: 'POST',
      mode: 'cors'
    });
    let data = null;
    fetch(request)
      .then(response => data = response.headers.get("Location"))
      .then(() => this.setState({location: data}))
      .then(() => console.log(data));
  }
}

export default App;