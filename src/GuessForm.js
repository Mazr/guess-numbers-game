import React, {Component} from 'react';

class GuessForm extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      hintList: [],
      guessAnswer: {
        answer: ''
      },
      guesses: []

    };
    this.setHintList = this.setHintList.bind(this);
  }

  render() {
    return (
      <form onSubmit={this.setHintList}>
        <fieldset>
          <legend>Guess Number:{this.props.location}</legend>
          <input maxLength={1} id={"1st-num"}/>
          <input maxLength={1} id={"2nd-num"}/>
          <input maxLength={1} id={"3rd-num"}/>
          <input maxLength={1} id={"4th-num"}/>
        </fieldset>
        {
          this.state.guesses.map(({hint, correct, answer}, index) => (
            <p>
              {answer + "    " + hint}
            </p>
          ))
        }
        <button type={"submit"}>Good Luck</button>
      </form>
    );
  }

  setHintList(e) {
    e.preventDefault();
    let str = this.getAnswer();
    // let dataList = null;
    this.setState({
      guessAnswer: {
        answer: str
      }
    }, () => {
      const URL = "http://localhost:8080" + this.props.location;
      let request = new Request(URL, {
        method: 'PATCH',
        mode: 'cors',
        body: JSON.stringify(this.state.guessAnswer),
        headers: {'Content-Type': 'application/json'}
      });
      fetch(request)
        .then(response => response.json()).then(respose => this.setState({
        guesses: [...this.state.guesses, {
          hint: respose.hint,
          correct: respose.correct,
          answer: str
        }]
      }));
    });
  }

  getAnswer() {
    let value1 = document.getElementById("1st-num").value;
    let value2 = document.getElementById("2nd-num").value;
    let value3 = document.getElementById("3rd-num").value;
    let value4 = document.getElementById("4th-num").value;
    let str = '';
    str += value1 + value2 + value3 + value4;
    return str;
  }
}

export default GuessForm;